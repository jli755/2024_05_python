#!/usr/bin/env python3

"""
Run this script to a directory with xml files exported out from archivist


Clean xml
    - clean text
    - clean new line
    - remove duplicated sequence label
"""

import time
import sys
import os
import re
from lxml import etree
import pandas as pd


def clean_text(input_dir, output_dir):
    """
    Go through text files in rootdir
        - pass 1: fixing repeating amps: &amp;amp;...amp;#digits; --> &#digits;
             - i.e. don&amp;amp;amp;#39;t --> don&#39;t
        - pass 2: fixing repeating amps: &amp;amp;...amp; --> &amp;
             - i.e. A&amp;amp;amp;E --> A&amp;E
        - pass 3: special case &#160: --> &#160;
        - pass 4: special case &#163< --> &#163;<
    """

    try:
        files = [f for f in os.listdir(input_dir) if os.path.isfile(os.path.join(input_dir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for input_filename in files:
        filename = os.path.join(output_dir, input_filename)
        print(filename + r": pass 1 fixing repeat amps: '&amp;amp;...amp;#xx;' -> '&#xx;")

        tmpfile1 = filename + ".temp1"
        tmpfile2 = filename + ".temp2"
        tmpfile3 = filename + ".temp3"

        with open( os.path.join(input_dir, input_filename) , "r") as fin:
            with open(tmpfile1, "w") as fout:
                for line in fin:
                    # regex: at least one 'amp;' in \1 and digits in \2
                    # replace all amp; right in front of a digit with only one &
                    # i.e. don&amp;amp;amp;#39;t --> don&#39;t
                    fout.write(re.sub(r'\&(amp;)+#(\d+);', r'&#\2;', line))

        print(filename + ": pass 2 fixing multiple '&amp;amp;amp;amp;'")
        with open(tmpfile1, "r") as fin:
            with open(tmpfile2, "w") as fout:
                for line in fin:
                    # A&amp;amp;E --> A&amp;E
                    fout.write(re.sub(r'\&(amp;)+', r'&amp;', line))

        print(filename + ": pass 3 fixing '&#160:' (note colon)")
        with open(tmpfile2, "r") as fin:
            with open(tmpfile3, "w") as fout:
                for line in fin:
                    # &#160: with &#160;
                    fout.write(re.sub(r"(&#[0-9]+):", r"\1;", line))

        print(filename + r": pass 4 fixing '&#163<' (note < tag start")
        with open(tmpfile3, "r") as fin:
            with open(filename, "w") as fout:
                for line in fin:
                    fout.write(line.replace("&#163<", "&#163;<"))

        # remove tmp
        print(filename + ": deleting tmpfile")
        os.unlink(tmpfile1)
        os.unlink(tmpfile2)
        os.unlink(tmpfile3)


def clean_newline(rootdir):
    """
    Overwrite the original xml file with a middle of context line break replaced by a space.
    Also expand the escaped characters, for example: &#163; becomes £
    Line breaks in text are generally represented as:
        \r\n - on a windows computer
        \r   - on an Apple computer
        \n   - on Linux
    """

    try:
        files = [f for f in os.listdir(rootdir) if os.path.isfile(os.path.join(rootdir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for filename in files:
        filename = os.path.join(rootdir, filename)
        print(filename)
        p = etree.XMLParser(resolve_entities=True)
        with open(filename, "rt") as f:
            tree = etree.parse(f, p)

        for node in tree.iter():
            if node.text is not None:
                if re.search("\n|\r|\r\n", node.text.rstrip()):
                    node.text = node.text.replace("\r\n", " ")
                    node.text = node.text.replace("\r", " ")
                    node.text = node.text.replace("\n", " ")

        # because encoding="UTF-8" in below options, the output can contain non-ascii characters, e.g.
        tree.write(filename, encoding="UTF-8", xml_declaration=True)


def rm_duplicate_n(rootdir):
    """
    Remove text <duplicate [n]> from sequence labels in the XML after it has exported
    """

    try:
        files = [f for f in os.listdir(rootdir) if os.path.isfile(os.path.join(rootdir, f))]
    except WindowsError:
        print("something is wrong")
        sys.exit(1)

    for filename in files:
        # get file name and extension
        (only_name, only_extension) = os.path.splitext(filename)

        # adding the new name with extension
        new_base = only_name + '_rm_duplicate_n' + only_extension
        new_filename = os.path.join(rootdir, new_base)

        p = etree.XMLParser(resolve_entities=True)
        with open(os.path.join(rootdir, filename), "rt") as f:
            tree = etree.parse(f, p)

        check = []
        for node in tree.iter():
            if node.text is not None:
                check_duplicate = bool(re.search(r" duplicate \d+$", node.text))
                check.append(check_duplicate)
                node.text = re.sub(r" duplicate \d+$", "", node.text)

        if any(check) is True:
            tree.write(new_filename, encoding="UTF-8", xml_declaration=True)


def main():

    input_dir = "/home/jenny/Documents/Jenny_ucl/2024_04_30_clean_xml/example_not_cleaned_1"
    output_dir = "/home/jenny/Documents/Jenny_ucl/2024_04_30_clean_xml/example_cleaned_output_1"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    clean_text(input_dir, output_dir)
    clean_newline(output_dir)
    rm_duplicate_n(output_dir)


if __name__ == "__main__":
    main()

