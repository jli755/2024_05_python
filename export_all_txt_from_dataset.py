#!/usr/bin/env python3

"""
Python 3: Web scraping using selenium
    - Download tv.txt and dv.txt files from Archivist Datasets, rename: prefix.tvlinking.txt, prefix.dv.txt
"""

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from urllib.parse import urlsplit, urlunsplit

from mylib import get_driver, url_base, archivist_login, get_base_url

import pandas as pd
import requests
import time
import sys
import os

n = 5 #seconds


def download_link(url_location, output_file):
    """
    Given a URL, using requests library to download it to output_file
    """
    print("Download: " + url_location)
    url_get = requests.get(url_location)
    url_get.raise_for_status()
    with open(output_file, "wb") as f: 
        f.write(url_get.content)
    # first word is the prefix
    return url_get.text.split('\t')[0]


def archivist_download_all_txt(main_dir, uname, pw, df, token_m):
    """
    Ddownloading everything from /admin/datasets
    """

    driver = get_driver()
    # log in to https://closer-archivist.herokuapp.com/admin/datasets/
    base_url = "https://closer-archivist.herokuapp.com/"
    print("Logging into host {}".format( base_url ))
    ok = archivist_login(driver, base_url, uname, pw)
    if not ok:
        print(f"Failed to login to {base_url}: skipping")

    print(f"Logged into {base_url}")
    url = "https://closer-archivist.herokuapp.com/admin/datasets/"
    print("Load '/admin/datasets' page : " + url)
    driver.get(url)
    delay = 10*n   #seconds 

    for index, row in df.iterrows():
        d_id = row['ID']
        d_name = row['Name']
        print(d_id, d_name)

        output_prefix_dir = os.path.join(main_dir, "closer-archivist_from_dataset", "txt_by_prefix")
        if not os.path.exists(output_prefix_dir):
            os.makedirs(output_prefix_dir)
        output_type_dir = os.path.join(main_dir, "closer-archivist_from_dataset", "txt_by_type")
        if not os.path.exists(output_type_dir):
            os.makedirs(output_type_dir)

        text_list_file = os.path.join(os.path.dirname(output_prefix_dir), "text_list.csv")
        if not os.path.isfile(text_list_file) :
            with open(text_list_file, "a") as f:
                f.write( ",".join(["ID", "Name", "Data"]) + "\n")

        # from dataset: Variables, Topic Variables(TV), Derived Variables (DV)

        # Topic Variables
        output_tv_dir = os.path.join(output_type_dir, "tvlinking",)
        if not os.path.exists(output_tv_dir):
            os.makedirs(output_tv_dir)

        print("Getting tv.txt")
        tv_location = os.path.join("https://closer-archivist.herokuapp.com/datasets", str(d_id), "tv.txt?token=" + token_m)

        try:
            tv_get = requests.get(tv_location)
            tv_get.raise_for_status()
            prefix = tv_get.text.split('\t')[0]

            output_dir = os.path.join(output_prefix_dir, prefix)
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            out_tv_prefix = os.path.join(output_dir, prefix + ".tvlinking.txt")
            out_tv_type = os.path.join(output_tv_dir, prefix + ".tvlinking.txt")
            
            with open(out_tv_prefix, "wb") as f:
                f.write(tv_get.content)
            with open(out_tv_type, "wb") as f:
                f.write(tv_get.content)

            with open(text_list_file, "a") as f:
                f.write( ",".join([str(d_id), prefix, "TV"]) + "\n")

        except requests.exceptions.RequestException as e:
            # print(e.request)
            # print(e.response)
            print("HTTPError")

        # Variables
        output_variables_dir = os.path.join(output_type_dir, "Variables",)
        if not os.path.exists(output_variables_dir):
            os.makedirs(output_variables_dir)

        print("Getting Variables.txt")
        variables_location = os.path.join("https://closer-archivist.herokuapp.com/datasets", str(d_id), "variables.txt?token=" + token_m)

        out_variables_prefix = os.path.join(output_dir, prefix + ".variables.txt")
        out_variables_type = os.path.join(output_variables_dir, prefix + ".variables.txt")

        try:
            variables_get = requests.get(variables_location)
            variables_get.raise_for_status()
                        
            with open(out_variables_prefix, "wb") as f:
                f.write(variables_get.content)
            with open(out_variables_type, "wb") as f:
                f.write(variables_get.content)

            with open(text_list_file, "a") as f:
                f.write( ",".join([str(d_id), prefix, "Variables"]) + "\n")
            
        except requests.exceptions.RequestException as e:
            # print(e.request)
            # print(e.response)
            print("HTTPError")

        # Derived Variables
        output_dv_dir = os.path.join(output_type_dir, "dv",)
        if not os.path.exists(output_dv_dir):
            os.makedirs(output_dv_dir)

        print("Getting dv.txt")
        dv_location = os.path.join("https://closer-archivist.herokuapp.com/datasets", str(d_id), "dv.txt?token=" + token_m)

        out_dv_prefix = os.path.join(output_dir, prefix + ".dv.txt")
        out_dv_type = os.path.join(output_dv_dir, prefix + ".dv.txt")
        try:
            dv_get = requests.get(dv_location)
            dv_get.raise_for_status()

            with open(out_dv_prefix, "wb") as f:
                f.write(dv_get.content)
            with open(out_dv_type, "wb") as f:
                f.write(dv_get.content)
            
            with open(text_list_file, "a") as f:
                f.write( ",".join([str(d_id), prefix, "DV"]) + "\n")

        except requests.exceptions.RequestException as e:
            # print(e.request)
            # print(e.response)
            print("HTTPError")

    driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    main_dir = "export_all_txt"
    if not os.path.exists(main_dir):
        os.makedirs(main_dir)

    # get a list of all ID/Name
    out_file = os.path.join(main_dir, "DatasetsExports.txt")
    #get_all_id(uname, pw, out_file)

    df = pd.read_csv(out_file, sep="\t")
    df["Name"] = df["Name"].str.replace("/"," ")
    df["Name"] = df["Name"].str.replace("'","")

    # get all text files
    token_m = "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6NTAsImFwaV9rZXkiOiJkOTk2Y2NiYjdiMzY4ZDgzMjQyNiJ9.F3dYWW8OkVP-QJIHyujRMselOQldLrvXGn-C2VP5AK8"
    archivist_download_all_txt(main_dir, uname, pw, df, token_m)


if __name__ == "__main__":
    main()
