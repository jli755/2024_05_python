#!/usr/bin/env python3

"""
Python 3: Web scraping using selenium
    - QV (Question Variables) qv.txt, rename prefix.qvmapping.txt
    - TQ (Topic Questions) tq.txt, rename prefix.tqlinking.txt
"""

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from urllib.parse import urlsplit, urlunsplit

from mylib import get_driver, url_base, archivist_login, get_base_url

import pandas as pd
import requests
import time
import sys
import os

n = 5 #seconds

def download_link(url_location, output_file):
    """
    Given a URL, using requests library to download it to output_file
    """
    print("Download: " + url_location)
    url_get = requests.get(url_location)
    url_get.raise_for_status()
    with open(output_file, "wb") as f: 
        f.write(url_get.content)


def get_all_id(uname, pw, out_file):
    """
    Go to /admin/datasets page, get all id/prefix
    """ 
    driver = get_driver()
    base_url = "https://closer-archivist.herokuapp.com/"
    print("Logging into host {}".format( base_url ))
    ok = archivist_login(driver, base_url, uname, pw)
    if not ok:
        print(f"Failed to login to {base_url}: skipping")
    delay = 20*n
    
    url = "https://closer-archivist.herokuapp.com/admin/datasets/"
    print("Load '/admin/datasets' page : " + url)
    driver.get(url)
    delay = 50*n
    
    # choose from dropdown to display all results on one page
    print("choose from dropdown to display all results on one page")
    #select = Select(driver.find_element(By.XPATH, "//select[@aria-label='rows per page']"))
    
    select = Select(driver.find_element(By.XPATH, value="/html/body/div/div/div/div/main/div[2]/div[1]/div/div[2]/table/tfoot/tr/td/div/div[2]/select"))

    # select by visible text
    select.select_by_visible_text('All')

    # locate id and link
    trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")
    print("The datasets page has {} rows, searching for matching row".format(len(trs)))

    # column 1 is "ID"
    for tr in trs:
        tr_row = tr.find_elements(by=By.XPATH, value="td")
        tr_id = tr_row[0].text
        tr_name = tr_row[1].text
        # data_export_page_url = os.path.join( base_url, 'admin/datasets', tr_id, "exports")

        with open(out_file, "w") as f: 
            f.write( ",".join([tr_id, tr_name]) + "\n")
        # append a line to out_file"""
        with open(out_file, "a") as f:
            f.write( ",".join(["ID", "Name"]) + "\n")
    driver.quit()


def archivist_download_all_txt(main_dir, uname, pw, df, token_m):
    """
    Downloading everything from /admin/datasets
    """

    driver = get_driver()
    # log in to https://closer-archivist.herokuapp.com/admin/instruments/exports
    base_url = "https://closer-archivist.herokuapp.com/"
    print("Logging into host {}".format( base_url ))
    ok = archivist_login(driver, base_url, uname, pw)
    if not ok:
        print(f"Failed to login to {base_url}: skipping")

    print(f"Logged into {base_url}")
    """
    url = "https://closer-archivist.herokuapp.com/admin/datasets/"
    # url = "https://closer-archivist.herokuapp.com/admin/instruments/exports"
    print("Load '/admin/datasets' page : " + url)
    driver.get(url)
    """
    delay = 30*n   #seconds 
    
    # we get the session cookie from Selenium, to get all them do:
    # print(driver.get_cookies())
    # we need the cookies for Requests, used below
    c1 = driver.get_cookie("_archivist_session")
    c2 = driver.get_cookie("XSRF-TOKEN")
    cookies_dict = {c1["name"]: c1["value"], c2["name"]: c2["value"]}
    
    for index, row in df.iterrows():
        d_id = row['ID']
        prefix = row['Prefix']
        print(d_id, prefix)

        output_prefix_dir = os.path.join(main_dir, "closer-archivist_txt_from_instrument", "txt_by_prefix")
        if not os.path.exists(output_prefix_dir):
            os.makedirs(output_prefix_dir)
        output_type_dir = os.path.join(main_dir, "closer-archivist_txt_from_instrument", "txt_by_type")
        if not os.path.exists(output_type_dir):
            os.makedirs(output_type_dir)

        text_list_file = os.path.join(os.path.dirname(output_prefix_dir), "text_list.csv")
        if not os.path.isfile(text_list_file) :
            with open(text_list_file, "a") as f:
                f.write( ",".join(["ID", "Prefix", "Type"]) + "\n")

        # from dataset: Variables, Topic Variables(TV), Derived Variables (DV)
        # from instrument: 
        output_dir = os.path.join(output_prefix_dir, prefix)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # Question Variables
        output_qv_dir = os.path.join(output_type_dir, "qv",)
        if not os.path.exists(output_qv_dir):
            os.makedirs(output_qv_dir)

        print("Getting qv.txt")
        qv_location = os.path.join("https://closer-archivist.herokuapp.com/instruments", str(d_id), "qv.txt?token=" + token_m)
        print(qv_location)

        out_qv_instrument = os.path.join(output_dir, prefix + ".qvmapping.txt")
        out_qv_type = os.path.join(output_qv_dir, prefix + ".qvmapping.txt")

        try:
            qv_get = requests.get(qv_location, cookies=cookies_dict)
            qv_get.raise_for_status()

            with open(out_qv_instrument, "wb") as f:
                f.write(qv_get.content)
            with open(out_qv_type, "wb") as f:
                f.write(qv_get.content)
          
            with open(text_list_file, "a") as f:
                f.write( ",".join([str(d_id), prefix, "QV"]) + "\n")

        except requests.exceptions.RequestException as e:
            # print(e.request)
            # print(e.response)
            print("HTTPError")

        # Topic Questions
        output_tq_dir = os.path.join(output_type_dir, "tq",)
        if not os.path.exists(output_tq_dir):
            os.makedirs(output_tq_dir)

        print("Getting tq.txt")
        tq_location = os.path.join("https://closer-archivist.herokuapp.com/instruments", str(d_id), "tq.txt?token=" + token_m)
        print(tq_location)

        out_tq_instrument = os.path.join(output_dir, prefix + ".tqlinking.txt")
        out_tq_type = os.path.join(output_tq_dir, prefix + ".tqlinking.txt")

        try:
            tq_get = requests.get(tq_location, cookies=cookies_dict)
            tq_get.raise_for_status()

            with open(out_tq_instrument, "wb") as f:
                f.write(tq_get.content)
            with open(out_tq_type, "wb") as f:
                f.write(tq_get.content)
          
            with open(text_list_file, "a") as f:
                f.write( ",".join([str(d_id), prefix, "TQ"]) + "\n")

        except requests.exceptions.RequestException as e:
            # print(e.request)
            # print(e.response)
            print("HTTPError")

    driver.quit()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    main_dir = "export_all_txt"
    if not os.path.exists(main_dir):
        os.makedirs(main_dir)

    # get a list of all ID/Name
    out_file = os.path.join(main_dir, "InstrumentExports.txt")
    #get_all_id(uname, pw, out_file)

    df = pd.read_csv(out_file, sep="\t")

    # get all text files
    token_m = "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6NTAsImFwaV9rZXkiOiJhNWM2MDVhMzRhNjYyOGZlN2E4YyJ9.aO97_CVBQJxGgQhgIRxS6yvJkA4opM8C9D_fT3bunKs"
    archivist_download_all_txt(main_dir, uname, pw, df, token_m)


if __name__ == "__main__":
    main()
